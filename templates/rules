#!/usr/bin/make -f

# Copyright (c) 2014-2015, djcj <djcj@gmx.de>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


include debian/confflags

DEB_HOST_GNU_CPU ?= $(shell dpkg-architecture -qDEB_HOST_GNU_CPU)

LIBPREFIX  = usr/lib/games
ICONPREFIX = $(LIBPREFIX)/
ICONPKG    = $(NAME)
ifeq ($(DATAPACKAGE), yes)
ICONPREFIX = usr/share/games/
ICONPKG    = $(NAME)-data
endif
ICONDEST   = $(CURDIR)/debian/$(ICONPKG)/usr/share
libdir     = $(CURDIR)/debian/$(NAME)/$(LIBPREFIX)/$(NAME)/
datadir    = $(CURDIR)/debian/$(NAME)-data/usr/share/games/$(NAME)/
plugindir  = source/$(NAME)_Data/Plugins


%:
	dh ${@}


override_dh_auto_install:
	mkdir -p $(libdir)
	mkdir -p $(ICONDEST)
	mkdir -p $(CURDIR)/debian/$(NAME)/usr/games
	mkdir -p $(CURDIR)/debian/$(NAME)/usr/share/applications

	# create icons
	debian/make-icons.sh $(ICON) $(ICONDEST)

	# install files
	cp -r source/* $(libdir)
	install -m644 $(NAME).desktop $(CURDIR)/debian/$(NAME)/usr/share/applications
	dh_link -p$(NAME) $(LIBPREFIX)/$(NAME)/$(NAME) usr/games/$(NAME)

ifeq ($(DATAPACKAGE), yes)
	# remove data package files from /$(LIBPREFIX)
	rm -rf $(libdir)$(NAME)_Data

	mkdir -p $(datadir)
	mkdir -p $(libdir)$(NAME)_Data
	mkdir -p $(CURDIR)/debian/$(NAME)-data/usr/share

	# install data package files into /usr/share
	cp -r source/$(NAME)_Data $(datadir)
	cp -r source/$(NAME)_Data/Mono $(libdir)$(NAME)_Data
	cd $(datadir)$(NAME)_Data && rm -rf Mono Plugins
	[ ! -d $(plugindir) ] || cp -r $(plugindir) $(libdir)$(NAME)_Data

	# create symbolic links between /usr/share and /usr/lib
	for f in $$(ls $(datadir)$(NAME)_Data) ; \
	do \
	    dh_link -p$(NAME) usr/share/games/$(NAME)/$(NAME)_Data/$$f $(LIBPREFIX)/$(NAME)/$(NAME)_Data/$$f ; \
	done
endif

	# replace UnityPlayer.png with a symbolic link
	# to the biggest icon available
	for d in 512 256 128 96 64 48 32 24 22 16 ;                                         \
	do                                                                                  \
	    dir="$(ICONDEST)/icons/hicolor/$${d}x$${d}" ;                                   \
	    if [ -d $$dir ] ;                                                               \
	    then                                                                            \
	        biggesticon="$$(basename $$dir)" ;                                          \
	        dh_link -p$(ICONPKG) usr/share/icons/hicolor/$$biggesticon/apps/$(NAME).png \
	            $(ICONPREFIX)$(NAME)/$(NAME)_Data/Resources/UnityPlayer.png ;           \
	        exit 0 ;                                                                    \
	    fi ;                                                                            \
	done


override_dh_installdocs:
	dh_installdocs -A debian/USLA debian/USLA.pdf debian/USLA.rtf


# Unity engine games don't provide any shared libraries
override_dh_makeshlibs:


override_dh_shlibdeps:
ifeq ($(PATCHELF),yes)
	# remove unnecessary dependencies that a binary was errerously linked against
	# by deleting their DT_NEEDED entries from the ELF header

	$(MAKE) -C $(CURDIR)/debian/patchelf

	rm -f uselessdeps.log
	(dh_shlibdeps -- --warnings=2 2>&1 | tee uselessdeps.log) > /dev/null

	lines=$$(wc -l uselessdeps.log | cut -d' ' -f1) ;                                 \
	for n in $$(seq 1 $$lines) ;                                                      \
	do                                                                                \
	    dep=$$(cut -d' ' -f15 uselessdeps.log | sed -n $${n}p) ;                      \
	    bin=$$(cut -d' ' -f10 uselessdeps.log | sed -n $${n}p) ;                      \
	    $(CURDIR)/debian/patchelf/patchelf --debug --remove-needed $$dep $$bin 2>&1 | \
	        grep -v -e 'keeping' | grep -v -e 'Kernel' ;                              \
	done ;
endif
ifeq ($(ARCH), x86)
	dh_shlibdeps -l/usr/lib/i386-linux-gnu/mesa/:/lib/i386-linux-gnu:/usr/lib/i386-linux-gnu
else
	dh_shlibdeps -l/usr/lib/x86_64-linux-gnu/mesa/:/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu
endif


override_dh_gencontrol:
	dh_gencontrol

# fix architecture when packaging i386 (amd64 only)
ifeq ($(ARCH), x86)
ifeq ($(DEB_HOST_GNU_CPU), x86_64)
	sed -i 's/Architecture: amd64/Architecture: i386/g' debian/$(NAME)/DEBIAN/control
endif
endif

# fix architecture when packaging amd64 (i386 only)
ifeq ($(ARCH), x86_64)
ifneq (, $(filter i386 i486 i586 i686 pentium,$(DEB_HOST_GNU_CPU)))
	sed -i 's/Architecture: i386/Architecture: amd64/g' debian/$(NAME)/DEBIAN/control
endif
endif


override_dh_builddeb:
	dh_builddeb $(arch_only) -- -Z$(Z) -z9

